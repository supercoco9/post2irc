class Ircbot
    def self.start(server, port, channels, nickname)
        @@channels = channels.split(',')
        @@bot = Cinch::Bot.new do
          configure do |c|
            c.nick = nickname
            c.user = nickname
            c.realname = nickname
            c.server = server
            c.port = port
            c.channels = @@channels
          end
        end
        Thread.new do
          @@bot.start
        end
    end

    def self.report(message)
        @@channels.each do |cname|
            @@bot.Channel(cname).send "#{message}"
        end
    end
end

