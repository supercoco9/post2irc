
class SinatraListener < Sinatra::Base
  UNAUTHORIZED = [ 401, 'unauthorized']

  def self.start(bind='0.0.0.0',port='4567',token='public')
    @@token = token
    configure :production, :development do
      enable :logging
      set :port, port
      set :bind, bind
    end
    self.run!
  end

  def string_cleaner(message)
      #remove all non printable characters and replace with a dot
      message.gsub /[^[:print:]]/m, "."
  end

  def authorized?(prefix)
    @@token == prefix
  end

  def talk(service, parts)
    parts.insert(0,service)
    Ircbot.report string_cleaner( parts.compact.join(' :: ') )
  end

  post "/:prefix" do
    halt UNAUTHORIZED unless authorized?(params[:prefix])
    talk 'custom', [params[:q]]
  end

  post "/:prefix/sources/bitbucket/service" do
    halt UNAUTHORIZED unless authorized?(params[:prefix])
    unless params[:payload].nil?
      payload=JSON.parse params[:payload]
      puts payload
      messages_authors=payload['commits'].map{|c| "#{c['message']} (#{c['author']})"}.join(',') rescue nil
      base_uri=payload['canon_url']
      repository_uri=payload['repository']['absolute_url']
      repository_name=payload['repository']['name']

      talk 'bitbucket', [ repository_name, messages_authors, "#{base_uri}#{repository_uri}"]
    end
  end

  post "/:prefix/sources/pivotal/webhook" do
    halt UNAUTHORIZED unless authorized?(params[:prefix])
    activity = XmlSimple.xml_in request.body.read

    event_type = activity['event_type']
    author = activity['author']
    description = activity['description']
    #we get the URLs and remove the /services/v3 so they will be accessed via direct link
    stories_urls =activity['stories'].map{|s1| s1['story'].map{|s2| s2['url'] } }.flatten.join(' , ').gsub('/services/v3','') rescue nil

    talk 'pivotal', [event_type, author, description, stories_urls]
  end
end

