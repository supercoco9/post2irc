# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "post2irc/version"

Gem::Specification.new do |s|
  s.name        = "post2irc"
  s.version     = Post2irc::VERSION
  s.authors     = ["javier ramirez"]
  s.email       = ["javier@formatinternet.com"]
  s.homepage    = "http://javier-ramirez.com"
  s.summary     = %q{Post to irc by sending a regular http post to a URL}
  s.description = %q{This gem will start a sinatra script that will listen to posts and will publish the contents to an irc channel. Simple token authentication is provided. built-in integrations for bitbucket and pivotal tracker web hooks}

  s.rubyforge_project = "post2irc"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_dependency "sinatra"
  s.add_dependency "cinch"
  s.add_dependency "json"
  s.add_dependency "xml-simple"
  s.required_ruby_version = '>= 1.9.1'
end

